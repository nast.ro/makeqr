type GlobalState = {
  autoEncode: boolean,
  saveContent: boolean,
  scale: number
}

function getNumberFromLocalStorage(name: string, defaultValue: number): number {
  const value = localStorage.getItem(name)
  if (value === null) return defaultValue;
  const parsed = parseInt(value);
  if (isNaN(parsed)) return defaultValue;
  return parsed;
}

let globalState = {
  autoGenerate: window.localStorage.getItem('auto-encode') === 'true',
  saveContent: window.localStorage.getItem('save-content') !== 'false',
  scale: getNumberFromLocalStorage('scale', 1)
}

export type { GlobalState };

export default globalState;