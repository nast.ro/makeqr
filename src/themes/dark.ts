import { createTheme } from "@fluentui/react";

let darkTheme = createTheme({
  palette: {
    themePrimary: '#348529',
    themeLighterAlt: '#f4faf3',
    themeLighter: '#d4ebd1',
    themeLight: '#b2daad',
    themeTertiary: '#73b66a',
    themeSecondary: '#44933a',
    themeDarkAlt: '#2f7725',
    themeDark: '#27651f',
    themeDarker: '#1d4a17',
    neutralLighterAlt: '#323130',
    neutralLighter: '#31302f',
    neutralLight: '#2f2e2d',
    neutralQuaternaryAlt: '#2c2b2a',
    neutralQuaternary: '#2a2928',
    neutralTertiaryAlt: '#282726',
    neutralTertiary: '#c8c8c8',
    neutralSecondary: '#d0d0d0',
    neutralPrimaryAlt: '#dadada',
    neutralPrimary: '#ffffff',
    neutralDark: '#f4f4f4',
    black: '#f8f8f8',
    white: '#323130',
  },
  semanticColors: {
    inputForegroundChecked: '#ffffff',
    primaryButtonText: '#ffffff',
    primaryButtonTextHovered: '#ffffff',
    primaryButtonTextDisabled: '#ffffff',
    primaryButtonTextPressed: '#ffffff'
  }
});

darkTheme.name = 'dark';

export default darkTheme;