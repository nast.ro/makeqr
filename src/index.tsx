import App from './components/app';
import { registerIcons } from '@fluentui/react';
import { h, FunctionalComponent } from 'preact';
import * as Themes from './themes';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// initialize icons
import { faAngleDown, faAngleUp, faCheck, faCircleInfo, faCircleXmark, faCode, faFileArrowDown, faGear, faInfo, faShieldHalved, faUserShield, faXmark } from '@fortawesome/free-solid-svg-icons'
registerIcons({
  icons: {
    'angle-down': <FontAwesomeIcon icon={faAngleDown} />,
    'angle-up': <FontAwesomeIcon icon={faAngleUp} />,
    'check': <FontAwesomeIcon icon={faCheck} />,
    'circle-info': <FontAwesomeIcon icon={faCircleInfo} />,
    'circle-xmark': <FontAwesomeIcon icon={faCircleXmark} />,
    'code': <FontAwesomeIcon icon={faCode} />,
    'file-arrow-down': <FontAwesomeIcon icon={faFileArrowDown} />,
    'gear': <FontAwesomeIcon icon={faGear} />,
    'info': <FontAwesomeIcon icon={faInfo} />,
    'shield-halved': <FontAwesomeIcon icon={faShieldHalved} />,
    'user-shield': <FontAwesomeIcon icon={faUserShield} />,
    'xmark': <FontAwesomeIcon icon={faXmark} />,

    // the following icons are used by fluentui components
    'cancel': <FontAwesomeIcon icon={faXmark} />,
    'checkmark': <FontAwesomeIcon icon={faCheck} />,
    'chevrondown': <FontAwesomeIcon icon={faAngleDown} />,
    'chevrondownsmall': <FontAwesomeIcon icon={faAngleDown} />,
    'chevronupsmall': <FontAwesomeIcon icon={faAngleUp} />,
    'clear': <FontAwesomeIcon icon={faXmark} />,
    'errorbadge': <FontAwesomeIcon icon={faCircleXmark} />
  }
});

// add plausible analytics script if enabled
if (window.localStorage.getItem('allow-analytics') === 'true') {
  let analyticsScript = document.createElement('script');
  analyticsScript.src = 'https://plausible.io/js/plausible.js';
  analyticsScript.dataset.domain = window.location.hostname;
  document.head.appendChild(analyticsScript);
}

let themeWrappedApp: FunctionalComponent = () => {
  return (
    <Themes.MediaQueryThemeProvider themes={{ dark: Themes.dark, light: Themes.light }}>
      <App />
    </Themes.MediaQueryThemeProvider>
  );
};

// hide preload
let preload = document.getElementById('preload');
if (preload != null) preload.style.display = 'none';

export default themeWrappedApp;