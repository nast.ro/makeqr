import { mergeStyles, PrimaryButton, useTheme, DefaultButton, MessageBar, MessageBarType } from '@fluentui/react';
import monaco from 'monaco-editor';
import Editor from '@monaco-editor/react';
import { Component, Fragment, h, createRef, RefObject } from 'preact';
import QRCode from 'qrcode';

import Header from './header';
import PrivacySetup from './privacy-setup';
import AppSetup from './app-setup';
import globalState from '../global-state';
import AboutPane from './about-pane';

export default class App extends Component<{}, { hash: string, error: string | null }> {
  state = {
    hash: '',
    error: null
  };

  _sourceEditorRef = createRef<monaco.editor.IStandaloneCodeEditor>();
  _resultCanvasRef = createRef<HTMLCanvasElement>();
  
  constructor() {
    super();
  }

  loadSavedData(name: string, editorRef: RefObject<monaco.editor.IStandaloneCodeEditor>) {
    let sessionValue = window.sessionStorage.getItem(name);
    if (sessionValue === null) {
      let localValue = window.localStorage.getItem(name);
      if (localValue !== null) editorRef.current?.setValue(localValue);
    } else {
      editorRef.current?.setValue(sessionValue);
      window.sessionStorage.removeItem(name);
    }
  }

  onError = (ex: any, operation: string | undefined = undefined) => {
    let prefix = '';
    if (typeof operation === 'string') prefix = `${operation} failed: `;
    console.error(ex);
    if (typeof ex === 'string') {
      this.setState({ error: `${prefix}${ex}` });
    } else if (ex instanceof Error) {
      this.setState({ error: `${prefix}${ex.message}` });
    } else if (ex === null) {
      this.setState({ error: `${prefix}null` });
    } else {
      this.setState({ error: `${prefix}${ex.toString()}` });
    }
  }

  onGlobalError = (e: ErrorEvent) => {
    console.error(e);
    this.onError(e.error);
  }

  onSourceEditorMount = (editor: monaco.editor.IStandaloneCodeEditor, _: any) => {
    editor.updateOptions({ wordWrap: 'on' });
    this._sourceEditorRef.current = editor;
    this.loadSavedData('source', this._sourceEditorRef);
  }

  onSourceEditorChange = (value: string | undefined, _2: monaco.editor.IModelContentChangedEvent) => {
    if (globalState.saveContent && typeof value === 'string') window.localStorage.setItem('source', value);
    if (globalState.autoGenerate) {
      this.onGenerateButtonClick();
    }
  }

  onGenerateButtonClick = () => {
    let source = this._sourceEditorRef.current?.getValue();
    if (typeof source === 'string' && source !== '') {
      try {
        QRCode.toCanvas(this._resultCanvasRef.current, source, (error) => {
          if (error) this.onError(error, 'Generation');
        })
      } catch (ex) {
        this.onError(ex, 'Generation');
      }
    } else {
      if (this._resultCanvasRef.current !== null) {
        this._resultCanvasRef.current.width = 0;
        this._resultCanvasRef.current.height = 0;
      }
    }
  }

  onGenerateImage = () => {
    let source = this._sourceEditorRef.current?.getValue();
    if (typeof source === 'string') {
      try {
        QRCode.toDataURL(source, { scale: globalState.scale }, (error, url) => {
          if (error) this.onError(error, 'Generation');
          let a = document.createElement('a');
          a.href = url;
          a.download = 'qrcode.png';
          a.click();
        })
      } catch (ex) {
        this.onError(ex, 'Image generation');
      }
    }
  }

  onClearSourceButtonClick = () => {
    this._sourceEditorRef.current?.setValue('');
  }

  onDismissErrorMessage = () => {
    this.setState({ error: null });
  }

  onResize = () => {
    if (this._sourceEditorRef !== null) this._sourceEditorRef.current?.layout({} as monaco.editor.IDimension);
  }

  componentDidMount() {
    window.addEventListener('resize', this.onResize);
    window.addEventListener('error', this.onGlobalError);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResize);
    window.addEventListener('error', this.onGlobalError);
  }

  render() {
    const theme = useTheme();

    return (
      <>
        <div
          className={mergeStyles(theme, {
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            minHeight: '100%',
            backgroundColor: theme.semanticColors.bodyBackground
          })}
        >
          <AboutPane />
          <AppSetup />
          <PrivacySetup onBeforeRefresh={() => {
            return new Promise<void>((resolve, _) => {
              let source = this._sourceEditorRef.current?.getValue();
              if (typeof source === 'string') window.sessionStorage.setItem('source', source);
              resolve();
            });
          }} />
          {this.state.error !== null &&
            <MessageBar
              className={mergeStyles(theme, {
                position: 'fixed',
                top: 50,
                left: 0,
                width: '100%',
                zIndex: 999
              })}
              messageBarType={MessageBarType.error}
              onDismiss={this.onDismissErrorMessage}
            >{this.state.error}</MessageBar>
          }
          <Header />
          <div className={mergeStyles(theme, {
            position: 'absolute',
            top: 50,
            left: 0,
            width: '100%',
            minHeight: 'calc(100% - 50px)',
            backgroundColor: theme.semanticColors.bodyBackground,
            display: 'flex',
            flexDirection: 'column'
          })}>
            <Editor wrapperProps={{className: mergeStyles(theme, { flexGrow: 1 })}} height={0} onMount={this.onSourceEditorMount} onChange={this.onSourceEditorChange} theme={theme.name === 'dark' ? 'vs-dark' : 'vs'} />
            <div className={mergeStyles(theme, {
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'flex-end',
              padding: 10
            })}>
              <DefaultButton text='Clear' onClick={this.onClearSourceButtonClick} style={{ marginRight: 10 }} />
              <PrimaryButton
                text='Generate'
                onClick={this.onGenerateButtonClick}
                split={true}
                menuProps={{
                  items: [
                    {
                      key: 'generate-image',
                      text: 'Generate image',
                      iconProps: { iconName: 'file-arrow-down' },
                      onClick: this.onGenerateImage
                    },
                  ]
                }}
              />
            </div>
            <div className={mergeStyles(theme, {
              flexGrow: 1,
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              height: 0
            })}><canvas ref={this._resultCanvasRef} className={mergeStyles(theme, {
              height: '100%',
              width: '100%'
            })}></canvas></div>
          </div>
        </div>
      </>
    );
  }
}