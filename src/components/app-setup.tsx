import { Checkbox, Panel, PrimaryButton, mergeStyles, useTheme, SpinButton } from '@fluentui/react';
import { Component, Fragment, h } from 'preact';
import globalState from '../global-state';

export default class AppSetup extends Component<{}, { visible: boolean }> {
  state = {
    visible: false
  };

  constructor() {
    super();
  }

  onHashChange = () => {
    let hash = window.location.hash;
    if (hash.startsWith('#')) hash = hash.substring(1);
    this.setState({ visible: hash.toLowerCase() === 'setup' });
  }

  componentDidMount() {
    window.addEventListener('hashchange', this.onHashChange);
    this.onHashChange();
  }

  componentWillUnmount() {
    window.removeEventListener('hashchange', this.onHashChange);
  }

  onAutoGenerateChange = (_: any, checked: boolean | undefined) => {
    if (typeof checked === 'boolean') {
      globalState.autoGenerate = checked;
      window.localStorage.setItem('auto-generate', checked ? 'true' : 'false');
    }
    this.setState({});
  }

  onSaveContentChange = (_: any, checked: boolean | undefined) => {
    if (typeof checked === 'boolean') {
      globalState.saveContent = checked;
      window.localStorage.setItem('save-content', checked ? 'true' : 'false');
      if (!checked) {
        window.localStorage.removeItem('source');
      }
    }
    this.setState({});
  }

  onScaleChange = (_: any, value: string | undefined) => {
    if (typeof value === 'string') {
      if (value === null) return;
      const parsed = parseInt(value);
      if (isNaN(parsed)) return;
      globalState.scale = parsed;
      window.localStorage.setItem('scale', value);
    }
    this.setState({});
  }

  onDismiss = () => {
    window.history.back();
  }

  renderFooter = () => {
    return (
      <>
        <PrimaryButton onClick={this.onDismiss}>Close</PrimaryButton>
      </>
    );
  }

  render() {
    const theme = useTheme();

    const checkboxStyle = mergeStyles(theme, {
      paddingTop: 10,
      paddingBottom: 10
    });

    return (
      <Panel
        headerText='Settings'
        isOpen={this.state.visible}
        onDismiss={this.onDismiss}
        onRenderFooterContent={this.renderFooter}
      >
        <>
          <Checkbox label="Instant generation" checked={globalState.autoGenerate} onChange={this.onAutoGenerateChange} className={checkboxStyle} />
          <Checkbox label="Save content" checked={globalState.saveContent} onChange={this.onSaveContentChange} className={checkboxStyle} />
          <SpinButton label="Scale (only affects codes generated as image)" min={1} max={100} step={1} defaultValue={globalState.scale.toString()} onChange={this.onScaleChange} />
        </>
      </Panel>
    );
  }
}