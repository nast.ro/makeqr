console.log('creating license bundle file');
const licensesList = require('licenses-list-generator');
const fs = require('fs');
let licenseFile = fs.openSync('src/assets/licenses.txt', 'w');
let isFirst = true;
let endsWithNewline = false;
for (let license of licensesList()) {
  if (isFirst) {
    isFirst = false;
  } else {
    if (!endsWithNewline) { fs.writeSync(licenseFile, '\n'); }
    fs.writeSync(licenseFile, '\n--------------------------------------------------------------------------------\n\n');
  }
  fs.writeSync(licenseFile, `${license.name} ${license.version}\nLicense: ${license.type}\n\n${license.text}`);
  if (license.text.endsWith('\n')) {
    endsWithNewline = true;
  }
}
fs.closeSync(licenseFile);
console.log('finished creating license bundle file');
